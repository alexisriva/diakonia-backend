from django.db import models
from django.utils.translation import gettext_lazy as _


class DateModel(models.Model):
    """
    Date information
    """
    created_at = models.DateTimeField(_('date_created'), auto_now_add=True)
    modified_at = models.DateTimeField(_('date_modified'), auto_now=True)

    class Meta:
        abstract = True


class BaseModel(DateModel):
    name = models.CharField(_('name'), max_length=255)
    is_active = models.BooleanField(_('active'), default=True)

    class Meta:
        abstract = True


class StatusModel(DateModel):
    is_active = models.BooleanField(_('active'), default=True)

    class Meta:
        abstract = True
