from rest_framework.permissions import BasePermission


class IsAdmin(BasePermission):
    """
    The request is authenticated as an admin
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            request.user.is_authenticated and
            request.user.profile and
            request.user.profile.is_admin
        )
