from django.db import models
from django.utils.translation import gettext_lazy as _

from core.models import BaseModel


class Provider(BaseModel):
    identifier = models.CharField(_('identifier'), max_length=15)
    address = models.CharField(_('address'), max_length=255)
    city = models.CharField(_('city'), max_length=100, blank=True)
    phone = models.CharField(_('phone number'), max_length=10, blank=True)

    class Meta:
        verbose_name = _('provider')
        verbose_name_plural = _('providers')

    def __str__(self):
        return self.name
