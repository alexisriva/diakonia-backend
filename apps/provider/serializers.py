from rest_framework.serializers import ModelSerializer

from .models import Provider


class ProviderSerializer(ModelSerializer):
    class Meta:
        model = Provider
        exclude = ('created_at', 'modified_at', 'is_active',)
