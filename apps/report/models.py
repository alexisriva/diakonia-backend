from django.db import models
from django.utils.translation import gettext_lazy as _

from core.models import DateModel
from apps.order.models import Order, OrderProduct
from apps.client.models import Client
from apps.provider.models import Provider


class Report(DateModel):
    pass
