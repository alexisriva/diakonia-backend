from django.db import models
from django.utils.translation import gettext_lazy as _


class Profile(models.Model):
    """
    Aditional user information
    """
    user = models.OneToOneField(
            'auth.User',
            on_delete=models.PROTECT,
            primary_key=True,
    )
    phone = models.CharField(_('phone'), max_length=10, blank=True, null=True)
    ADMIN = 0
    EMPLOYEE = 1
    ROLE_CHOICES = (
        (ADMIN, 'Administrador'),
        (EMPLOYEE, 'Empleado'),
    )
    role = models.PositiveSmallIntegerField(_('role'), choices=ROLE_CHOICES, default=EMPLOYEE)

    class Meta:
        default_related_name = 'profile'
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')

    def __str__(self):
        return '{} ({})'.format(self.user.get_full_name(), self.ROLE_CHOICES[self.role][1])

    @property
    def is_admin(self):
        return self.role == self.ADMIN

    @property
    def is_employee(self):
        return self.role == self.EMPLOYEE
