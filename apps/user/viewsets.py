from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _

from rest_framework import status, viewsets
from rest_framework.response import Response

from core.permissions import IsAdmin

from .models import Profile
from .serializers import UserDetailSerializer, UserSerializer, ProfileSerializer


User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.filter(is_staff=False)
    permission_classes = (IsAdmin,)
    serializer_class = UserDetailSerializer
    serializer_action_classes = {
        'list': UserSerializer,
    }

    def get_serializer_class(self):
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()
    

    def create(self, request):
        try:
            first_name = request.data.get('first_name', '')
            last_name = request.data.get('last_name', '')
            username = request.data.get('username', '')
            profile = request.data.get('profile', '')
            phone = profile.get('phone')
            role = profile.get('role')

            user_data = {
                'first_name': first_name,
                'last_name': last_name
            }

            user = User.objects.create_user(username, **user_data)
            user.save()

            profile, created = Profile.objects.get_or_create(user=user)
            profile.phone = phone
            profile.role = role
            profile.save()

            serializer = UserDetailSerializer(user)
            
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            print(e.__str__())
            return Response(e.__str__(), status=status.HTTP_400_BAD_REQUEST)


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    permission_classes = (IsAdmin,)
    serializer_class = ProfileSerializer
