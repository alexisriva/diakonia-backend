from django.contrib.auth import get_user_model

from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Profile


User = get_user_model()


class ProfileSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = ('phone', 'role')


class AuthUserSerializer(ModelSerializer):
    token = SerializerMethodField(read_only=True)
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'token', 'profile')

    def get_token(self, obj):
        return self.context.get('token', '')


class UserSerializer(ModelSerializer):
    full_name = SerializerMethodField('get_full_name')
    profile = ProfileSerializer()

    def get_full_name(self, user):
        return user.get_full_name()

    class Meta:
        model = User
        fields = ('id', 'username', 'full_name', 'profile')


class UserDetailSerializer(ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'profile')
