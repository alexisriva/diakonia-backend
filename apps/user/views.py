import json

from django.contrib.auth import authenticate, get_user_model

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny

from .serializers import AuthUserSerializer


User = get_user_model()


def set_success_response(response, user, msg):
    token, _ = Token.objects.get_or_create(user=user)
    serializer = AuthUserSerializer(user, context={'token': token.key})

    response['data'] = serializer.data
    response['error'] = False
    response['msg'] = msg

    return response


class LoginView(APIView):
    """
    API endpoint for user login
    """
    permission_classes = [AllowAny,]

    def post(self, request):
        response = {
            'error': True,
            'msg': 'Parámetros erróneos',
            #'data': None,
        }
        http_status = status.HTTP_400_BAD_REQUEST

        try:
            data = json.loads(request.body)

            username = data.get('username', '')
            password = data.get('password', '')

            user = authenticate(username=username, password=password)

            if user:
                response = set_success_response(response, user, 'Inicio de sesión exitoso')
                http_status = status.HTTP_200_OK

        except Exception as e: print(e.__str__())
        return Response(response, http_status)


class LogoutView(APIView):
    """
    API endpoint for user login
    """
    
    def post(self, request):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)


class SignUpView(APIView):
    """
    API endpoint for user creation
    """
    permission_classes = []

    def post(self, request):
        response = {
            'error': True,
            'msg': 'Parámetros erróneos',
        }
        http_status = status.HTTP_400_BAD_REQUEST

        try:
            data = json.loads(request.body)

            first_name = data.get('first_name', '')
            last_name = data.get('last_name', '')
            username = data.get('username', '')
            password = data.get('password', '')

            user = User.objects.filter(username=username).exists()

            if user:
                response['msg'] = 'El usuario ya se encuentra registrado'
            else:
                user_data = {}
                if first_name:
                    user_data['first_name'] = first_name
                if last_name:
                    user_data['last_name'] = last_name

                user = User.objects.create_user(username, password=password, **user_data)
                user.save()

                response = set_success_response(response, user, 'Usuario creado correctamente')
                http_status = status.HTTP_200_OK

        except: pass
        return Response(response, http_status)


login_view = LoginView.as_view()
logout_view = LogoutView.as_view()
signup_view = SignUpView.as_view()
