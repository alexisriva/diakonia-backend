from rest_framework.serializers import ModelSerializer, SerializerMethodField

from apps.user.serializers import UserSerializer
from apps.client.serializers import ClientSerializer
from apps.product.serializers import ProductSerializer

from .models import Order, OrderProduct


class OrderProductSerializer(ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = OrderProduct
        fields = ('id', 'product', 'count')


class OrderProductDownloadSerializer(ModelSerializer):
    location = SerializerMethodField('get_product_location')
    product = SerializerMethodField('get_product_name')

    def get_product_location(self, order_product):
        return order_product.product.location

    def get_product_name(self, order_product):
        return order_product.product.name
    
    class Meta:
        model = OrderProduct
        fields = ('product', 'count', 'location')


class OrderSerializer(ModelSerializer):
    class Meta:
        model = Order
        exclude = ('modified_at',)


class OrderDetailSerializer(ModelSerializer):
    products = OrderProductSerializer(many=True)
    user = UserSerializer()
    modified_by = UserSerializer()
    institution = ClientSerializer()

    class Meta:
        model = Order
        fields = ('id', 'user', 'modified_by', 'institution', 'status', 'created_at', 'products')
