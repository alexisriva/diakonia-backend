from django.db import models
from django.utils.translation import gettext_lazy as _

from core.models import DateModel
from apps.product.models import Product
from apps.client.models import Client


class Order(DateModel):
    """
    Dispatch Order
    """
    user = models.ForeignKey(
            'auth.User',
            on_delete=models.PROTECT,
            verbose_name=_('user'),
            related_name='orders',
    )
    modified_by = models.ForeignKey(
            'auth.User',
            on_delete=models.PROTECT,
            verbose_name=_('modified by'),
            related_name='+',
    )
    institution = models.ForeignKey(Client, on_delete=models.PROTECT, related_name='orders')
    CREATED = 1
    VALIDATED = 10
    STATUS_CHOICES = (
        (CREATED, 'Creada'),
        (VALIDATED, 'Validada'),
    )
    status = models.PositiveIntegerField(_('status'), choices=STATUS_CHOICES,
            default=CREATED)

    class Meta:
        verbose_name = _('order')
        verbose_name_plural = _('orders')

    def __str__(self):
        return 'Order #{}'.format(self.id)


class OrderProduct(DateModel):
    """
    Many to many relation between orders and products
    """
    order = models.ForeignKey(Order, on_delete=models.PROTECT, related_name='products')
    product = models.ForeignKey(Product, on_delete=models.PROTECT, related_name='+')
    count = models.PositiveIntegerField(_('count'))

    class Meta:
        verbose_name = _('order product')
        verbose_name_plural = _('order products')

    def __str__(self):
        return str.format("Order #{} | {}: {}", self.order.id, self.product.name, self.count)
