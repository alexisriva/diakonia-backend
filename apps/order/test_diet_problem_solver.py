import unittest
from diet_problem_solver import generate_order

class TestGenerateOrder(unittest.TestCase):

    def test_case_1(self):
        products = {'CAJA GALLETA CIRCUS MARACUYA 12 DISPLAY 12 UND DE 28 GR C/U': [28, '15-Nov-20', 'Galletas', 'Panaderia', 'D-2-1', '7.14', '12', '28', 'CORDIALSA'], 'CAJA GALLETAS APETITAS ANIS 10 UND DE 400 GR C/U': [128, '18-Nov-20', 'Galletas', 'Panaderia', 'C-6-1', '13.39', '10', '400', 'SUPERIOR'], 'CAJA GALLETAS TUYAS LECHE  15 DISPLAYS DE 18 UND DE 252 GR C/U': [80, '20-Nov-20', 'Galletas', 'Panaderia', 'C-6-3', '7.14', '15', '252', 'SUPERIOR'] }
        categories = ['Galletas']
        population = 100
        days = 7
        self.assertEqual(generate_order(products, categories, population, days), {'CAJA GALLETAS TUYAS LECHE  15 DISPLAYS DE 18 UND DE 252 GR C U': 8})

    def test_case_2(self):
        products = 'Hola mundo'
        categories = ['Galletas']
        population = 100
        days = 7
        self.assertEqual(generate_order(products, categories, population, days), {})

    def test_case_3(self):
        products = {1: 0.35}
        categories = ['Galletas']
        population = 100
        days = 7
        self.assertEqual(generate_order(products, categories, population, days), {})

    def test_case_4(self):
        products = {'CAJA GALLETA CIRCUS MARACUYA 12 DISPLAY 12 UND DE 28 GR C/U': [28, '15-Nov-20', 'Galletas', 'Panaderia','D-2-1', '7.14', '12', '28', 'CORDIALSA'],'CAJA GALLETAS APETITAS ANIS 10 UND DE 400 GR C/U': [128, '18-Nov-20', 'Galletas', 'Panaderia', 'C-6-1','13.39', '10', '400', 'SUPERIOR'],'CAJA GALLETAS TUYAS LECHE  15 DISPLAYS DE 18 UND DE 252 GR C/U': [80, '20-Nov-20', 'Galletas', 'Panaderia','C-6-3', '7.14', '15', '252','SUPERIOR']}
        categories = 'Hola mundo'
        population = 100
        days = 7
        self.assertEqual(generate_order(products, categories, population, days), {})

    def test_case_5(self):
        products = {'CAJA GALLETA CIRCUS MARACUYA 12 DISPLAY 12 UND DE 28 GR C/U': [28, '15-Nov-20', 'Galletas', 'Panaderia','D-2-1', '7.14', '12', '28', 'CORDIALSA'],'CAJA GALLETAS APETITAS ANIS 10 UND DE 400 GR C/U': [128, '18-Nov-20', 'Galletas', 'Panaderia', 'C-6-1','13.39', '10', '400', 'SUPERIOR'],'CAJA GALLETAS TUYAS LECHE  15 DISPLAYS DE 18 UND DE 252 GR C/U': [80, '20-Nov-20', 'Galletas', 'Panaderia','C-6-3', '7.14', '15', '252','SUPERIOR']}
        categories = [1,2]
        population = 100
        days = 7
        self.assertEqual(generate_order(products, categories, population, days), {})

    def test_case_6(self):
        products = {'CAJA GALLETA CIRCUS MARACUYA 12 DISPLAY 12 UND DE 28 GR C/U': [28, '15-Nov-20', 'Galletas', 'Panaderia', 'D-2-1', '7.14', '12', '28', 'CORDIALSA'], 'CAJA GALLETAS APETITAS ANIS 10 UND DE 400 GR C/U': [128, '18-Nov-20', 'Galletas', 'Panaderia', 'C-6-1', '13.39', '10', '400', 'SUPERIOR'], 'CAJA GALLETAS TUYAS LECHE  15 DISPLAYS DE 18 UND DE 252 GR C/U': [80, '20-Nov-20', 'Galletas', 'Panaderia', 'C-6-3', '7.14', '15', '252', 'SUPERIOR'] }
        categories = ['Galletas']
        population = 'Hola mundo'
        days = 7
        self.assertEqual(generate_order(products, categories, population, days), {})

    def test_case_7(self):
        products = {'CAJA GALLETA CIRCUS MARACUYA 12 DISPLAY 12 UND DE 28 GR C/U': [28, '15-Nov-20', 'Galletas', 'Panaderia', 'D-2-1', '7.14', '12', '28', 'CORDIALSA'], 'CAJA GALLETAS APETITAS ANIS 10 UND DE 400 GR C/U': [128, '18-Nov-20', 'Galletas', 'Panaderia', 'C-6-1', '13.39', '10', '400', 'SUPERIOR'], 'CAJA GALLETAS TUYAS LECHE  15 DISPLAYS DE 18 UND DE 252 GR C/U': [80, '20-Nov-20', 'Galletas', 'Panaderia', 'C-6-3', '7.14', '15', '252', 'SUPERIOR'] }
        categories = ['Galletas']
        population = -100
        days = 7
        self.assertEqual(generate_order(products, categories, population, days), {})

    def test_case_8(self):
        products = {'CAJA GALLETA CIRCUS MARACUYA 12 DISPLAY 12 UND DE 28 GR C/U': [28, '15-Nov-20', 'Galletas', 'Panaderia', 'D-2-1', '7.14', '12', '28', 'CORDIALSA'], 'CAJA GALLETAS APETITAS ANIS 10 UND DE 400 GR C/U': [128, '18-Nov-20', 'Galletas', 'Panaderia', 'C-6-1', '13.39', '10', '400', 'SUPERIOR'], 'CAJA GALLETAS TUYAS LECHE  15 DISPLAYS DE 18 UND DE 252 GR C/U': [80, '20-Nov-20', 'Galletas', 'Panaderia', 'C-6-3', '7.14', '15', '252', 'SUPERIOR'] }
        categories = ['Galletas']
        population = 100
        days = 'Hola mundo'
        self.assertEqual(generate_order(products, categories, population, days), {})

    def test_case_9(self):
        products = {'CAJA GALLETA CIRCUS MARACUYA 12 DISPLAY 12 UND DE 28 GR C/U': [28, '15-Nov-20', 'Galletas', 'Panaderia', 'D-2-1', '7.14', '12', '28', 'CORDIALSA'], 'CAJA GALLETAS APETITAS ANIS 10 UND DE 400 GR C/U': [128, '18-Nov-20', 'Galletas', 'Panaderia', 'C-6-1', '13.39', '10', '400', 'SUPERIOR'], 'CAJA GALLETAS TUYAS LECHE  15 DISPLAYS DE 18 UND DE 252 GR C/U': [80, '20-Nov-20', 'Galletas', 'Panaderia', 'C-6-3', '7.14', '15', '252', 'SUPERIOR'] }
        categories = ['Galletas']
        population = 100
        days = -7
        self.assertEqual(generate_order(products, categories, population, days), {})

if __name__ == '__main__':
    unittest.main()