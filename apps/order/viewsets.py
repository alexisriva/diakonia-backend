from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _

from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from apps.client.models import Client
from apps.category.models import Subcategory

from .models import Order, OrderProduct
from .serializers import (OrderSerializer, OrderDetailSerializer,
                          OrderProductSerializer, OrderProductDownloadSerializer)
from .diet_problem_solver import generate_order, get_order_products


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderDetailSerializer
    serializer_action_classes = {
        'list': OrderDetailSerializer,
        'retrieve': OrderDetailSerializer,
        'update': OrderSerializer,
        'partial_update': OrderSerializer,
    }

    def get_serializer_class(self):
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()
    
    def create(self, request):
        try:
            institution = request.data['institution']
            inst = get_object_or_404(Client, pk=institution)

            days = int(request.data['days'])

            subcategories = request.data['subcategories']

            categories = []
            for subc in subcategories:
                s = Subcategory.objects.get(id=subc['id'])
                categories.append(s)

            population = inst.adult_population + inst.child_population
            products = generate_order(categories, population, days)

            data = {
                'user': request.user.pk,
                'modified_by': request.user.pk,
                'institution': institution
            }

            serializer = OrderSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            order = serializer.save()

            get_order_products(products, order)

            serializer = OrderDetailSerializer(order)

            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED,
                    headers=headers)
        except Exception as e:
            return Response(e.__str__(), status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None):
        order_status = request.data['status']

        order = get_object_or_404(Order, pk=pk)
        order.status = order_status
        order.modified_by = request.user
        order.save()
        
        serializer = OrderDetailSerializer(order)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        order = get_object_or_404(Order, pk=pk)
        order.products.all().delete()
        order.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(methods=['get'], detail=True)
    def download(self, request, pk=None):
        order = get_object_or_404(Order, pk=pk)
        serializer = OrderProductDownloadSerializer(order.products, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class OrderProductViewSet(viewsets.ModelViewSet):
    queryset = OrderProduct.objects.all()
    serializer_class = OrderProductSerializer

    def put(self, request):
        updated_instances = []
        order_products = request.data

        for order_product in order_products:
            instance = get_object_or_404(OrderProduct, pk=order_product['id'])
            instance.count = order_product['count']
            instance.save()
            updated_instances.append(instance)

        serializer = OrderProductSerializer(updated_instances, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
