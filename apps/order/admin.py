from django.contrib import admin

from .models import Order, OrderProduct


admin.site.register(OrderProduct)

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'status', 'created_at', 'modified_at')
