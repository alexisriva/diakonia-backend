import math
from pulp import *

from apps.product.models import Product
from .models import Order, OrderProduct
#from apps.category.models import Subcategory


prods = Product.objects.filter(is_active=True)


def categorize_data():
    granos = {}
    leche = {}
    bebidas_avena = {}
    yogurt = {}
    queso = {}
    galletas = {}
    panes_derivados = {}
    reposteria = {}
    cafe_te_chocolate = {}
    harinas = {}
    cereales = {}
    especies = {}
    sal = {}
    azucar = {}
    snacks = {}
    arroz = {}
    fideos_pastas = {}
    conservas_enlatados = {}
    golosinas = {}
    aceites_mantequillas = {}
    frutas = {}
    verduras = {}
    legumbres = {}
    frutos_secos = {}
    colas = {}
    energizantes = {}
    cerveza = {}
    agua = {}
    vinos = {}
    licores = {}
    jugos = {}
    higiene_aseo_hogar = {}
    higiene_aseo_personal = {}
    pescados_mariscos = {}
    pollo_carne_cerdo = {}
    electrodomesticos = {}
    articulos_bazar = {}
    kits = {}
    for product in prods:
        if product.weight and product.weight != 0 and product.weight is not None:
            if product.subcategory.name == 'Leche':
                leche[product.id] = float(product.weight)
            elif product.subcategory.name == 'Galletas':
                galletas[product.id] = float(product.weight)
            elif product.subcategory.name == 'Harinas':
                harinas[product.id] = float(product.weight)
            elif product.subcategory.name == 'Cereales':
                cereales[product.id] = float(product.weight)
            elif product.subcategory.name == 'Snacks':
                snacks[product.id] = float(product.weight)
            elif product.subcategory.name == 'Frutas':
                frutas[product.id] = float(product.weight)
            elif product.subcategory.name == 'Frutos secos':
                frutos_secos[product.id] = float(product.weight)
            elif product.subcategory.name == 'Higiene&Cuidado personal':
                higiene_aseo_personal[product.id] = float(product.weight)
            elif product.subcategory.name == 'Pescados y Mariscos':
                pescados_mariscos[product.id] = float(product.weight)
            elif product.subcategory.name == 'Pollo, Carne, Cerdo':
                pollo_carne_cerdo[product.id] = float(product.weight)
    return leche, galletas, harinas, cereales, snacks, frutas, frutos_secos, higiene_aseo_personal, \
           pescados_mariscos, pollo_carne_cerdo


def generate_order(categories, population, days):
    response = {}

    #DATOS DEL MODELO

    # food items
    food_items = []
    # categories_names
    categories_names = []

    # costs
    costs = {}
    for product in prods:
        costs[product.id] = float(product.price)

    #VALIDACION DEPENDIENDO DE LOS DATOS
    for category in categories:
        products = Product.objects.filter(subcategory=category).order_by("expiration_date")
        if products:
            for product in products:
                if product.units is None and product.weight is None:
                    response[product.id] = math.ceil(population / product.stock)
                    break
                elif product.units is not None:
                    response[product.id] = math.ceil(population / product.units)
                    break
                else:
                    food_items.append(product.id)
                    categories_names.append(category.name)
                    break

    if food_items:
        #MODELO MATEMATICO
        try:
            leche, galletas, harinas, cereales, snacks, frutas, frutos_secos, higiene_aseo_personal, \
            pescados_mariscos, pollo_carne_cerdo = categorize_data()

            #PROBLEMA
            prob = LpProblem("Order Problem", LpMinimize)

            #RESTRICCIONES
            food_vars = LpVariable.dicts("Food",food_items, lowBound=0, cat='Continuous')

            #Funcion a minimizar
            prob += lpSum([costs[i]*food_vars[i] for i in food_items])

            # Leche
            if 'Leche' in categories_names and bool(leche):
                prob += lpSum([leche[f] * food_vars[f] for f in food_items if f in leche]) >= \
                        (250.0 * population * days), "MinimoLeche"
            # Galletas
            if 'Galletas' in categories_names and bool(galletas):
                prob += lpSum([galletas[f] * food_vars[f] for f in food_items if f in galletas]) >= \
                        (40.0 * population * days), "MinimoGalletas"
            # Harinas
            if 'Harinas' in categories_names and bool(harinas):
                prob += lpSum([harinas[f] * food_vars[f] for f in food_items if f in harinas]) >= \
                        (50.0 * population * days), "MinimoHarinas"
            # Cereales
            if 'Cereales' in categories_names and bool(cereales):
                prob += lpSum([cereales[f] * food_vars[f] for f in food_items if f in cereales]) >= \
                        (30.0 * population * days), "MinimoCereales"
            # Snacks
            if 'Snacks' in categories_names and bool(snacks):
                prob += lpSum([snacks[f] * food_vars[f] for f in food_items if f in snacks]) >= \
                        (30.0 * population * days), "MinimoSnacks"
            # Frutas
            if 'Frutas' in categories_names and bool(frutas):
                prob += lpSum([frutas[f] * food_vars[f] for f in food_items if f in frutas]) >= \
                        (200.0 * population * days), "MinimoFrutas"
            # Frutos secos
            if 'Frutos secos' in categories_names and bool(frutos_secos):
                prob += lpSum([frutos_secos[f] * food_vars[f] for f in food_items if f in frutos_secos]) >= \
                        (30.0 * population * days), "MinimoFrutosSecos"
            # Pescados y mariscos
            if 'Pescados y Mariscos' in categories and bool(pescados_mariscos):
                prob += lpSum([pescados_mariscos[f] * food_vars[f] for f in food_items if f in pescados_mariscos]) >= \
                        (125.0 * population * days), "MinimoMariscos"
            # Pollo, carne y cerdo
            if 'Pollo, Carne, Cerdo' in categories_names and bool(pollo_carne_cerdo):
                prob += lpSum([pollo_carne_cerdo[f] * food_vars[f] for f in food_items if f in pollo_carne_cerdo]) >= \
                        (120.0 * population * days), "MinimoPolloCarneCerdo"

            #RESOLUCION DEL PROBLEMA
            prob.solve()
            # The status of the solution is printed to the screen
            print("Status:", LpStatus[prob.status])

            for v in prob.variables():
                if v.varValue > 0:
                    print(v.name)
                    product = v.name.split('_')
                    key = int(product[1])

                    response[key] = math.ceil(v.varValue)
                    print(key, "=", math.ceil(v.varValue))
            print(response)
            return response

        except:
            return response
    return response


def get_parsed_order(population, categories, days):
    response = ''
    products = generate_order(categories, population, days)
    for product, count in products.items():
        string = product + ':' + str(count) + '|'
        response += string

    return response[:-1]


def get_order_products(products, order):
    for product_id, count in products.items():
        prod = Product.objects.get(pk=product_id)
        if prod:
            OrderProduct.objects.create(order=order, product=prod, count=count)


# Example
#categorias = Subcategory.objects.filter(is_active=True)
#print(generate_order(categorias, 100, 7))
#get_parsed_order(100, categorias, 7)
