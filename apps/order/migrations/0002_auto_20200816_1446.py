# Generated by Django 3.1 on 2020-08-16 14:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='institution',
            field=models.CharField(max_length=255, verbose_name='institution'),
        ),
        migrations.AlterField(
            model_name='order',
            name='number',
            field=models.CharField(db_index=True, max_length=128, unique=True, verbose_name='order number'),
        ),
    ]
