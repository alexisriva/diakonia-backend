# Generated by Django 3.1 on 2020-08-30 01:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0002_auto_20200828_1810'),
        ('order', '0005_auto_20200829_2128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='institution',
            field=models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, related_name='orders', to='client.client'),
        ),
    ]
