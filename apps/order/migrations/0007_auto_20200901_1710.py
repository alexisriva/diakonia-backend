# Generated by Django 3.1 on 2020-09-01 17:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0002_auto_20200828_1810'),
        ('order', '0006_auto_20200830_0130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='institution',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='orders', to='client.client'),
        ),
    ]
