from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.category.models import Subcategory
from apps.provider.models import Provider

from core.models import BaseModel


class Product(BaseModel):
    batch = models.CharField(_('batch'), max_length=100)
    stock = models.PositiveIntegerField(_('stock'))
    expiration_date = models.DateField(_('expiration date'))
    location = models.CharField(_('location'), max_length=100)
    price = models.DecimalField(_('price'), max_digits=8, decimal_places=2)
    units = models.PositiveIntegerField(_('units per box'), blank=True, null=True)
    weight = models.DecimalField(_('weight (g)'), max_digits=6, decimal_places=2, blank=True, null=True)
    subcategory = models.ForeignKey(Subcategory, on_delete=models.PROTECT, related_name='products')
    provider = models.ForeignKey(Provider, on_delete=models.PROTECT, related_name='products')

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return self.name
