from rest_framework.serializers import ModelSerializer

from apps.category.serializers import SubcategorySerializer
from apps.provider.serializers import ProviderSerializer
from .models import Product


class ProductSerializer(ModelSerializer):
    subcategory = SubcategorySerializer
    provider = ProviderSerializer

    class Meta:
        model = Product
        fields = ('id', 'name', 'stock', 'expiration_date', 'location', 'price', 'units', 'weight', 'subcategory', 'provider',)
