from django.db import models
from django.utils.translation import gettext_lazy as _

from core.models import BaseModel


class Client(BaseModel):
    address = models.CharField(_('address'), max_length=255)
    phone = models.CharField(_('phone number'), max_length=10)
    adult_population = models.PositiveIntegerField(verbose_name='adult population')
    child_population = models.PositiveIntegerField(verbose_name='child population')

    class Meta:
        verbose_name = _('client')
        verbose_name_plural = _('clients')

    def __str__(self):
        return self.name
