from django.urls import path, include
from rest_framework import routers

from . import viewsets


router = routers.DefaultRouter()
router.register(r'categories', viewsets.CategoryViewSet)
router.register(r'subcategories', viewsets.SubcategoryViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
