from rest_framework.serializers import ModelSerializer

from .models import Category, Subcategory


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        exclude = ('is_active', 'created_at', 'modified_at',)


class SubcategorySerializer(ModelSerializer):
    category = CategorySerializer()
    class Meta:
        model = Subcategory
        exclude = ('is_active', 'created_at', 'modified_at',)
