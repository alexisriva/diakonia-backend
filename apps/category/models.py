from django.db import models
from django.utils.translation import gettext_lazy as _

from core.models import BaseModel


class Category(BaseModel):
    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.name


class Subcategory(BaseModel):
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    adult_portion = models.PositiveIntegerField(verbose_name='adult portion (g)', blank=True, null=True)
    child_portion = models.PositiveIntegerField(verbose_name='child portion (g)', blank=True, null=True)

    class Meta:
        verbose_name = _('Subcategory')
        verbose_name_plural = _('Subcategories')

    def __str__(self):
        return self.name

